package com.thoughtworks.rectangle;

import org.junit.jupiter.api.Test;

import static com.thoughtworks.rectangle.Rectangle.*;
import static org.hamcrest.Matchers.closeTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class RectangleTest {

    @Test
    void shouldReturnAreaOfRectangleWhenLengthAndBreadthIsPassed() {
        Rectangle rectangle = createRectangle(2.00,1.00);

        double area_actual=rectangle.area();

        assertThat(area_actual,is(closeTo(2,0.001)));

    }
    @Test
    void shouldReturnPerimeterOfRectangleWhenLengthAndBreadthIsPassed() {
        Rectangle rectangle = createRectangle(2.00,1.00);

        double area_actual=rectangle.perimeter();

        assertThat(area_actual,is(closeTo(6,0.001)));
    }
    @Test
    void shouldReturnAreaOfSquareWhenSideIsProvided() {
        Rectangle square = createSquare(2.00);

        double area_actual=square.area();

        assertThat(area_actual,is(closeTo(4,0.001)));

    }
    @Test
    void shouldReturnPerimeterOfSquareWhenSideIsProvided() {
        Rectangle square = createSquare(2.00);

        double area_actual=square.perimeter();

        assertThat(area_actual,is(closeTo(8,0.001)));
    }


}