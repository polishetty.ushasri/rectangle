package com.thoughtworks.square;

import org.junit.jupiter.api.Test;

import static org.hamcrest.Matchers.closeTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class SquareTest {
    @Test
    void shouldReturnAreaWhenSideIsProvided() {
        Square square = new Square(2.00);

        double actual = square.area();

        assertThat(4.00, is(closeTo(actual, 0.001)));
    }

    @Test
    void shouldReturnPerimeterWhenSideIsProvided() {
        Square square = new Square(2.00);

        double actual = square.perimeter();

        assertThat(8.00, is(closeTo(actual, 0.001)));
    }
}
